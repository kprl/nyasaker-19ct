<?php

if ( ! function_exists( 'pre_r' ) ) {
  function pre_r( $val, $die = 0 ) {
    echo "<pre>";
    print_r( $val );
    echo "</pre>";
    if ( $die == 1 ) { die(); }
  }
}

// Enqueue parent theme styles and child theme stylesheet
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function twentynineteen_child_setup() {

  require_once('functions/cpt_hk_product.php');

  // Add support for Block Styles
  add_theme_support( 'wp-block-styles' );

  add_theme_support(
    'custom-logo',
    array(
      'height'      => 250,
      'width'       => 750,
      'flex-width'  => true,
      'flex-height' => true,
    )
  );

  //add_theme_support( 'disable-custom-colors' );

  add_theme_support( 'editor-color-palette', array(
    array(
      'name' => __( 'Main', 'nyasaker19ct' ),
      'slug' => 'main',
      'color' => '#00A9E1',
    ),
    array(
      'name' => __( 'Complement', 'nyasaker19ct' ),
      'slug' => 'complement',
      'color' => '#F7931E',
    ),
    array(
      'name' => __( 'Light', 'nyasaker19ct' ),
      'slug' => 'light',
      'color' => '#ededed',
    ),
    array(
      'name' => __( 'Lighter', 'nyasaker19ct' ),
      'slug' => 'lighter',
      'color' => '#f7f7f7',
    ),
    array(
      'name' => __( 'Grey', 'nyasaker19ct' ),
      'slug' => 'grey',
      'color' => '#6c757d',
    ),
    array(
      'name' => __( 'Dark', 'nyasaker19ct' ),
      'slug' => 'dark',
      'color' => '#2b303b',
    ),
    array(
      'name' => __( 'Darker', 'nyasaker19ct' ),
      'slug' => 'darker',
      'color' => '#111111',
    ),
  ) );

}
add_action( 'after_setup_theme', 'twentynineteen_child_setup', 100 );

/** ACF **/

// Advancedcustomfields ACF settings and register
function my_acf_init() {
	acf_update_setting('remove_wp_meta_box', true);
	acf_update_setting('l10n', true);

  /** BLOCKS **/
  // check function exists
  if( function_exists('acf_register_block') ) {

    acf_register_block(array(
      'name'            => 'kprl-bootstrap-carousel',
      'title'           => __('kprl Bootstrap Carousel'),
      'description'		  => __('A bootstrap-carousel block.'),
      'render_callback' => 'kprl_hk_acf_block_render_callback',
      'category'        => 'layout',
      'icon'            => 'format-gallery',
      'keywords'        => array( 'carousel', 'bootstrap', 'bildspel', 'kprl' ),
    ));

    acf_register_block(array(
      'name'            => 'kprl-hk-products',
      'title'           => __('kprl Nya sakers produkter'),
      'description'		  => __('Block för att lista Nya sakers produkter.'),
      'render_callback' => 'kprl_hk_acf_block_render_callback',
      'category'        => 'layout',
      'icon'            => 'carrot',
      'keywords'        => array( 'Nya saker', 'bootstrap', 'produkt', 'kprl' ),
    ));
  }
}
add_action('acf/init', 'my_acf_init');

function kprl_hk_acf_block_render_callback( $block ) {

	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/block-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/block-{$slug}.php") );
	}
}

add_action('init', 'load_hk_acf_field_group');
function load_hk_acf_field_group() {
  if (function_exists('register_field_group')) {
		require_once('functions/hk_acf.php');
  }
}
