<?php

if ( ! function_exists('hk_products') ) {

// Register Custom Post Type
function hk_products() {

	$labels = array(
		'name'                  => _x( 'Produkter', 'Post Type General Name', 'nyasaker19ct' ),
		'singular_name'         => _x( 'Produkt', 'Post Type Singular Name', 'nyasaker19ct' ),
		'menu_name'             => __( 'Produkter', 'nyasaker19ct' ),
		'name_admin_bar'        => __( 'Produkter', 'nyasaker19ct' ),
		'archives'              => __( 'Produktarkiv', 'nyasaker19ct' ),
		'attributes'            => __( 'Produktattribut', 'nyasaker19ct' ),
		'parent_item_colon'     => __( 'Föräldraprodukt:', 'nyasaker19ct' ),
		'all_items'             => __( 'Alla produkter', 'nyasaker19ct' ),
		'add_new_item'          => __( 'Lägg till ny produkt', 'nyasaker19ct' ),
		'add_new'               => __( 'Lägg till ny', 'nyasaker19ct' ),
		'new_item'              => __( 'Ny produkt', 'nyasaker19ct' ),
		'edit_item'             => __( 'Redigera produkt', 'nyasaker19ct' ),
		'update_item'           => __( 'Uppdatera produkt', 'nyasaker19ct' ),
		'view_item'             => __( 'Visa produkt', 'nyasaker19ct' ),
		'view_items'            => __( 'Visa produkter', 'nyasaker19ct' ),
		'search_items'          => __( 'Sök produkt', 'nyasaker19ct' ),
		'not_found'             => __( 'Not found', 'nyasaker19ct' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'nyasaker19ct' ),
		'featured_image'        => __( 'Featured Image', 'nyasaker19ct' ),
		'set_featured_image'    => __( 'Set featured image', 'nyasaker19ct' ),
		'remove_featured_image' => __( 'Remove featured image', 'nyasaker19ct' ),
		'use_featured_image'    => __( 'Use as featured image', 'nyasaker19ct' ),
		'insert_into_item'      => __( 'Insert into item', 'nyasaker19ct' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'nyasaker19ct' ),
		'items_list'            => __( 'Items list', 'nyasaker19ct' ),
		'items_list_navigation' => __( 'Items list navigation', 'nyasaker19ct' ),
		'filter_items_list'     => __( 'Filter items list', 'nyasaker19ct' ),
	);
  $rewrite = array(
		'slug'                  => 'produkt',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Produkt', 'nyasaker19ct' ),
		'description'           => __( 'Kjälpmedelskatalogens produkter', 'nyasaker19ct' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
    'show_in_rest'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-carrot',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
    'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'hk_product', $args );

}
add_action( 'init', 'hk_products', 0 );

}
