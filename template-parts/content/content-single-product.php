<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php
	$cA = get_fields( get_the_ID() );
	if( $cA ):
?>



		<div class="entry-content row d-flex align-items-start">
			<div class="order-1 order-md-2 col-12 col-md-8 row align-items-start">

				<?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
				<header class="entry-header col-12">
					<span class="hk-presents"><?php echo "Nya saker presenterar..."; ?></span>
					<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
				</header>
				<?php endif; ?>
				<div class="col-12 col-lg-8">
					<?php
					the_content(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						)
					);

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
							'after'  => '</div>',
						)
					);
					?>
				</div>
				<div class="col-12 col-lg-4 content-sidebar row align-items-start">
					<?php if ( $cA['byline'] ) { ?>
						<figure class="wp-block-image alignwide col-6 col-md-6 col-sm-6 col-lg-12">
							<img src="<?php echo $cA['byline']['url']; ?>" alt="<?php echo $cA['byline']['title']; ?>" />
							<?php if ( $cA['byline']['title'] OR $cA['byline']['caption'] ) { ?>
								<figcaption class="figcaption-title"><?php echo $cA['byline']['title']; ?></figcaption><figcaption><?php echo $cA['byline']['caption']; ?></figcaption>
							<?php } ?>
						</figure>
					<?php } if ( $cA['logo'] ) { ?>
						<figure class="wp-block-image alignwide col-6 col-md-6 col-sm-6 col-lg-12">
							<img src="<?php echo $cA['logo']['url']; ?>" alt="<?php echo $cA['logo']['title']; ?>" />
							<?php if ( $cA['logo']['title'] OR $cA['logo']['caption'] ) { ?>
								<figcaption class="figcaption-title"><?php echo $cA['logo']['title']; ?></figcaption><figcaption><?php echo $cA['logo']['caption']; ?></figcaption>
							<?php } ?>
						</figure>
					<?php } if ( $cA['link'] ) { ?>
						<div class="wp-block-button alignwide col-6 col-md-6 col-sm-6 col-lg-12">
							<?php
								$buttontext = "Gå till ";
								if ( $cA['buttontext'] ) {
									$buttontext .= $cA['buttontext'];
								} else {
									$buttontext .= get_the_title();
								}
							?>
							<a class="wp-block-button__link" href="<?php echo $cA['link']; ?>" alt="<?php echo get_the_title(); ?>" target="_blank"><?php echo $buttontext; ?></a>
						</div>
					<?php } ?>
			  </div>
			</div>

			<div class="order-2 order-md-1 col-12 col-md-3 content-images">
		    <?php
				if ( $cA['images'] ) {
					foreach ($cA['images'] as $key => $image) {
						?>
							<figure class="wp-block-image float-left is-resized is-style-default alignwide">
								<img src="<?php echo $image['sizes']['medium_large']; ?>" alt="<?php echo $image['title']; ?>" />
								<?php if ( $image['caption'] ) { ?>
									<figcaption class="figcaption-title"><?php echo $image['title']; ?></figcaption><figcaption><?php echo $image['caption']; ?></figcaption>
								<?php } ?>
							</figure>
						<?php
					}
				}
				?>
		  </div>
		</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //twentynineteen_entry_footer(); ?>
	</footer><!-- .entry-footer -->

	<?php if ( ! is_singular( 'attachment' ) ) : ?>
	<?php get_template_part( 'template-parts/post/author', 'bio' ); ?>
	<?php endif; ?>
<?php endif; ?>

</article><!-- #post-${ID} -->
