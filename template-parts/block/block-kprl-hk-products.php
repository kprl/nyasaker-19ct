<?php
/**
 * Block Name: kprl Nya sakers produkter
 *
 * This is the template that displays the products.
 */

// get image field (array)
$fields = get_fields();

// create id attribute for specific styling
$id = 'products-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class  = $block['align'] ? 'align' . $block['align'] : '';
$css_class    = $block['className'];

$args = array( 'post_type'        => 'hk_product',
               'posts_per_page'   => -1,
               'status'           => 'published',
        );
$loop = new WP_Query( $args );

?>
<div id="<?php echo $id; ?>" class="hk-products card-columns <?php echo $align_class; ?> <?php echo $css_class; ?>">
  <?php

  while ( $loop->have_posts() ) : $loop->the_post();

    $cA = get_fields( get_the_ID() );
    if( $cA ):
  ?>
      <a href="<?php the_permalink(); ?>" alt="<?php echo get_the_title(); ?>">
        <div class="card shadow">
          <?php
            if ( $cA['alt_title'] ) {
              $title = $cA['alt_title'];
            } else {
              $title = get_the_title();
            }
          ?>
          <h5><?php echo $title; ?></h5>
          <div class="municipality"><?php echo $cA['municipality']; ?></div>
          <?php if ( $cA['short_description'] ) { ?>
          <div class="card-body">
            <p class="card-text"><?php echo $cA['short_description']; ?></p>
          </div>
          <?php } ?>
          <img src="<?php echo $cA['image']['sizes']['medium_large']; ?>" class="card-img" alt="<?php echo get_the_title(); ?>">
          <div class="card-footer">Läs mer</div>
        </div>
      </a>
  <?php
    endif;
  endwhile;
  wp_reset_query();
  ?>
</div>
